import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'

import { generateStore } from './redux/store'
import { App } from './components/App.js'

let store = generateStore()

let WithStore = () => <Provider store={store}> <App /> </Provider>

render(
    <WithStore />,
    document.getElementById('root')
)