import styled from 'styled-components'

export const Title = styled.h1`
    font-size : 1.9em;
    margin: 20px 10px 0px 10px;
    font-family: sans-serif;
`

export const List = styled.ul`
    list-style: none;
    padding: 0;
`