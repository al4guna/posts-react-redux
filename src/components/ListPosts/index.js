import React, { useEffect } from 'react'
import { connect } from 'react-redux'

import { 
    fetchPosts, 
    fetchPostsSuccess, 
    fetchPostsError } 
from '../../redux/postReducer'

import { Post } from '../Post/'
import { Title, List } from './styled'

const ListPosts = ({ postsList, fetchPosts }) => {  
    useEffect(() => {
       fetchPosts()
    }, []) 

    const { posts, loading } = postsList
    if(loading) {
        return <h1>Loading</h1>
    }

    return(
        <div>
            <Title>Posts</Title>
            
            <List> {
                posts.map((post, key) => {
                   return <Post key={key} {...post} />
                }) }
            </List>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        postsList: state.post.postsList
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchPosts : () => {
            dispatch(fetchPosts()).data.then((response) => {
                !response.error ? dispatch(fetchPostsSuccess(response.data)) : dispatch(fetchPostsError(response.error));
            })
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(ListPosts)


