import React from 'react'
import { 
    BrowserRouter  as Router,
    Switch,
    Route
} from 'react-router-dom'

import { Header } from './Header'
import { GlobalStyle } from './GlobalStyled'
import  ListPosts  from './ListPosts'
import { NewPost } from './NewPost'

export const App = () => {
    return(
        <div>
            <GlobalStyle />
            <Router>
                <Header />
                <Switch>
                    <Route exact path="/">
                        <ListPosts />
                    </Route>
                    <Route exact path="/new">
                        <NewPost />
                    </Route>
                    <Router exact path="post/:id">
                        
                    </Router>
                </Switch>
            </Router>
        </div>
    )
}