import React from 'react'
import { Link } from 'react-router-dom'

import { Container } from './styled'

export const Header = () => {
    return(
        <Container>
            <div>
                <Link to="/new">New Posts</Link>
            </div>
        </Container>
    )
}