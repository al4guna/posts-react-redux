import styled from 'styled-components'

export const Container = styled.header`
    display: flex;
    justify-content: flex-end;
    padding: 5px 30px;
    -webkit-box-shadow: 0px 1px 15px 0px rgba(0,0,0,0.39);
    -moz-box-shadow: 0px 1px 15px 0px rgba(0,0,0,0.39);
    box-shadow: 0px 1px 15px 0px rgba(0,0,0,0.39);
    margin: 0;
    padding: 20px 15px;
`