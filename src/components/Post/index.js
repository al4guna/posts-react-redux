import React from 'react'

import { Item, Text } from './styled'

export const Post = ({title}) => {
    return(
        <Item>
            <Text>{title}</Text>
        </Item>
    )
}