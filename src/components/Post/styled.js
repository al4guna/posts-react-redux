import styled from 'styled-components'

export const Item = styled.li`
    background: pink;
    padding: 10px 10px;
    border-radius: 5px;
    cursor: pointer;
    margin: 0px 10px 2px 10px;
`

export const Text = styled.p`
    font-size: 1em;
    font-family: sans-serif;
`