import { createStore, combineReducers } from 'redux'

import { postReducer } from './postReducer'

const rootReducer = combineReducers({
    post: postReducer
})

export const generateStore = () => {
    let store = createStore(
        rootReducer
    )
    return store;
}
