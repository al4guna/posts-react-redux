import axios from 'axios'

const URL = 'http://192.168.1.8:8888' 

//initialState
const INITIAL_STATE = { 
    postsList:   {  posts: [],  error: null, loading: false },  
    newPost:     {  post: null, error: null, loading: false }, 
    activePost:  {  post: null, error: null, loading: false }, 
    deletedPost: {  post: null, error: null, loading: false },
};

//actions
const FETCH_POSTS         = "FETCH_POST"
const FETCH_POSTS_SUCCESS = "FETCH_POSTS_SUCCESS"
const FETCH_POSTS_ERROR   = "FETCH_POSTS_ERROR"

const CREATE_POST         = "CREATE_POST"
const CREATE_POST_SUCCESS = "CREATE_POST_SUCCESS"
const CREATE_POST_ERROR   = "CREATE_POST_ERROR"

const UPDATE_POST = "UPDATE_POST"
const REMOVE_POST = "REMOVE_POST"



//reducer
export const postReducer = ( state = INITIAL_STATE, action = {} ) => {
    let error;
    switch(action.type) {
        case FETCH_POSTS :
            return { ...state, postsList: {posts:[], error: null, loading: true} };
        case FETCH_POSTS_SUCCESS: 
            return { ...state, postsList: {posts:action.data, error: null, loading: false} };
        case FETCH_POSTS_ERROR:
            error = action.data || {message: action.data.message};
            return { ...state, postsList: {posts: [], error: error, loading: false} };
       
        case CREATE_POST : 
            return { ...state, newPost: { ...state.newPost, loading: true} };
        case CREATE_POST_SUCCESS: 
            return { ...state, newPost: { post: action.data, error: null, loading: false} };
        case CREATE_POST_ERROR:
            error = action.payload || {message: action.payload.message};
            return { ...state, newPost: { post:null, error: error, loading: false} }

        case UPDATE_POST : ;
        case REMOVE_POST : ;
        default: 
            return state;
    }
}



//actions creators
export function fetchPosts() {
    const request = axios({
        method: 'get',
        url: `${URL}/posts`,
        headers:[]
    });

    return { 
        type: FETCH_POSTS, 
        data: request
    };
}

export function fetchPostsSuccess(posts) {
    return {
        type: FETCH_POSTS_SUCCESS,
        data: posts
    }
}

export function fetchPostsError(error) {
    return {
        type: FETCH_POSTS_ERROR,
        data: error
    }
}

export function loadTask() {
    return { type : LOAD };
}

export function createPost(post) {
    return { type : CREATE_POST, post}
}

export function updatePost(post) {
    return { type : UPDATE_POST, post }
}

export function removePost(post) {
    return { type : REMOVE_POST, post}
}