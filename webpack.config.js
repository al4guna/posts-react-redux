const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const jsRule = {
    test: /\.js$/,
    exclude: '/node_modules/',
    use: {
        loader: 'babel-loader',
        options: {
            presets: ['@babel/preset-env', '@babel/preset-react']
        }
    }
}

module.exports = {
    mode: 'development',
    entry: { 
        app: './src/index.js'
    },
    output: { 
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [jsRule]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'example redux',
            template: './src/index.html'
        })
    ]
}